﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.Threading;
using System.ComponentModel;
using PingAlert.Properties;

namespace PingAlert
{
    public partial class MessageDialog : Form
    {
        static int delay = 1000; // 1 second
        static double totalTime;

        public MessageDialog(string message, string action, string sender)
        {
            InitializeComponent();

            totalTime = Utility.ConvertTimer(Settings.Default.MessageTimer);

            RichTextBox objRichTextBoxMessage = (RichTextBox)richTextBox_message;
            this.Text = "(" + action + ") - " + sender;
            objRichTextBoxMessage.Text = message;
            backgroundWorker1.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker1_ProgressChanged);
            backgroundWorker1.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker1_RunWorkerCompleted);
        }

        private void MessageDialog_Load(object sender, EventArgs e)
        {
            const int margin = 0;
            int x = Screen.PrimaryScreen.WorkingArea.Right -
                this.Width - margin;
            int y = Screen.PrimaryScreen.WorkingArea.Bottom -
                this.Height - margin;
            this.Location = new Point(x, y);

            WinApi.MakeTopMost(this);

            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            int elapsed = 0;

            while (elapsed < totalTime)
            {
                if (backgroundWorker1.CancellationPending)
                {
                    e.Cancel = true;
                    return;
                }

                elapsed += delay;
                Thread.Sleep(delay);

                //  we receive the number of seconds elapsed
                double remaining = (double)totalTime - (double)elapsed;
                double div = (double)remaining / (double)totalTime;
                double result = div * 100;

                backgroundWorker1.ReportProgress((int)result);
            }

            e.Cancel = true;
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressBar_lifespan.Value = e.ProgressPercentage;
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            this.Close();
        }
    }
}

﻿using Microsoft.Win32;
using System.Windows.Forms;

namespace PingAlert
{
    class Utility
    {
        static RegistryKey rkApp = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

        public static bool StartupStatus()
        {
            if (rkApp.GetValue("PingAlert") == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static void DisableStartup()
        {
            if (rkApp.GetValue("PingAlert") != null)
            {
                rkApp.DeleteValue("PingAlert", false);
            }
        }

        public static void EnableStartup()
        {
            if (rkApp.GetValue("PingAlert") == null)
            {
                rkApp.SetValue("PingAlert", Application.ExecutablePath);
            }
        }

        public static string WhichServer(int value)
        {
            switch (value)
            {
                case 0: return "goonfleet.com";
                case 1: return "jabber-server.goonfleet.com";
                default:
                    return "goonfleet.com";
            }
        }

        public static int WhichPriority(int value)
        {
            switch (value)
            {
                case 0: return 10;
                case 1: return 0;
                case 2: return 8;
                case 3: return 7;
                case 4: return 6;
                case 5: return 5;
                case 6: return 4;
                case 7: return 3;
                case 8: return 2;
                case 9: return 1;
                case 10: return 0;
                case 11: return -1;
                case 12: return -2;
                case 13: return -3;
                case 14: return -4;
                case 15: return -5;
                case 16: return -6;
                case 17: return -7;
                case 18: return -8;
                case 19: return -9;
                case 20: return -10;
                default:
                    return 10;
            }
        }

        public static double ConvertTimer(double value)
        {
            switch (value)
            {
                case 0: return 30000;
                case 1: return 60000;
                case 2: return 120000;
                case 3: return 180000;
                case 4: return 240000;
                case 5: return 300000;
                case 6: return 360000;
                case 7: return 420000;
                case 8: return 480000;
                case 9: return 540000;
                case 10: return 600000;
                case 11: return 660000;
                case 12: return 720000;
                case 13: return 780000;
                case 14: return 840000;
                case 15: return 900000;
                case 16: return 960000;
                case 17: return 1020000;
                case 18: return 1080000;
                case 19: return 1140000;
                case 20: return 1200000;
                default:
                    return 10;
            }
        }

        public static string ConvertUsername(string username)
        {
            return username + "@goonfleet.com";
        }
    }
}
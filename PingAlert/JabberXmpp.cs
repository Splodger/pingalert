﻿using agsXMPP;
using agsXMPP.protocol.client;
using System;
using System.Diagnostics;

namespace PingAlert
{
    class JabberXmpp
    {
        private static string _password;
        private static string _server;
        private static int _priority;
        private static Jid _jidSender;
        private static XmppClientConnection xmpp;
        public event EventHandler<MessageEventArgs> RaiseMessageEvent;

        public JabberXmpp(string username, string password, string server, int priority)
        {
            _password = password;
            _server = server;
            _priority = priority;
            _jidSender = new Jid(username);
        }

        public void Initilize()
        {
            xmpp = new XmppClientConnection(_server);

            try
            {
                xmpp.Port = 5222;
                xmpp.UseSSL = true;
                xmpp.UseStartTLS = true;
                xmpp.KeepAlive = true;
                xmpp.Priority = _priority;
                xmpp.Resource = "PingAlert";

                xmpp.OnLogin += new ObjectHandler(OnLogin);
                xmpp.OnAuthError += new XmppElementHandler(OnAuthError);
                xmpp.OnMessage += new MessageHandler(OnMessage);
                xmpp.OnError += new ErrorHandler(OnError);

                xmpp.Open(_jidSender.User, _password);
            }
            catch (Exception e)
            {
                Debug.WriteLine("Exception Thrown");
                Debug.WriteLine(e.Message);
            }
        }

        public void Close()
        {
            xmpp.Close();
        }

        private void OnLogin(object sender)
        {
            Debug.WriteLine("Logged In");
            OnRaiseMessageEvent(new MessageEventArgs("Logged In", "Login", "System"));
        }

        private void OnError(object sender, Exception ex)
        {
            Close();
            Debug.WriteLine("Error");
            Debug.WriteLine(ex.ToString());
            OnRaiseMessageEvent(new MessageEventArgs("An error occured!", "Error", "System"));
        }

        private void OnAuthError(object o, agsXMPP.Xml.Dom.Element el)
        {
            Close();
            Debug.WriteLine("Login Failed");
            OnRaiseMessageEvent(new MessageEventArgs("Login Failed", "AuthError", "System"));
        }

        private void OnMessage(object sender, agsXMPP.protocol.client.Message msg)
        {
            if (msg.Body != null && msg.From.User == "directorbot")
            {
                Debug.WriteLine("Message Received");
                Debug.WriteLine(msg.Body.ToString(), msg.From.User.ToString());
                OnRaiseMessageEvent(new MessageEventArgs(msg.Body.ToString(), "Ping", msg.From.User.ToString()));
            }
        }

        protected virtual void OnRaiseMessageEvent(MessageEventArgs e)
        {
            RaiseMessageEvent?.Invoke(this, e);
        }
    }

    public delegate void MessageEventHandler(object sender, MessageEventArgs a);

    public class MessageEventArgs : EventArgs
    {
        private string _message;
        private string _action;
        private string _sender; 

        public MessageEventArgs(string message, string action, string sender)
        {
            _message = message;
            _action = action;
            _sender = sender;
        }
        
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public string Action
        {
            get { return _action; }
            set { _action = value; }
        }

        public string Sender
        {
            get { return _sender; }
            set { _sender = value; }
        }
    }
}

﻿using PingAlert.Properties;
using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows.Forms;

namespace PingAlert
{
    public partial class Main : Form
    {
        public static string SetValueForError;
        private JabberXmpp client;

        public Main()
        {
            InitializeComponent();
            AutoLogin();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void AutoLogin()
        {
            if (!String.IsNullOrEmpty(Settings.Default.Username) && !String.IsNullOrEmpty((Settings.Default.Password)) && !backgroundWorker_Xmpp.IsBusy)
            {
                backgroundWorker_Xmpp.RunWorkerAsync();
            }
        }

        private void loginToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form configuration = new UserConfiguration
            {
                StartPosition = FormStartPosition.CenterScreen,
                Visible = true
            };
        }

        private void backgroundWorker_Xmpp_DoWork(object sender, DoWorkEventArgs e)
        {
            client = new JabberXmpp(Utility.ConvertUsername(Settings.Default.Username), Settings.Default.Password, Utility.WhichServer(Settings.Default.Server), Utility.WhichPriority(Settings.Default.Priority));
            client.Initilize();
            client.RaiseMessageEvent += HandleMessageEvent;

            if (backgroundWorker_Xmpp.CancellationPending)
            {
                e.Cancel = true;
                Debug.WriteLine("Background worker stopped!");
                notifyIcon1.Icon = Resources.beeRed;
                notifyIcon1.Text = "PingAlert - Disconnected";
                return;
            }
        }

        private void backgroundWorker_Xmpp_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
        }

        private void backgroundWorker_Xmpp_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Cancelled)
            {
                notifyIcon1.Icon = Resources.beeRed;
                notifyIcon1.Text = "PingAlert - Disconnected";
            }
            else if(e.Error != null)
            {
                notifyIcon1.Icon = Resources.beeRed;
                notifyIcon1.Text = "PingAlert - Disconnected";
            }
            else
            {
                notifyIcon1.Icon = Resources.beeRed;
                notifyIcon1.Text = "PingAlert - Disconnected";
            }
        }

        private void connectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Settings.Default.Username) && String.IsNullOrEmpty((Settings.Default.Password)))
            {
                SetValueForError = "Username/Password are required!";
                Form configuration = new UserConfiguration
                {
                    StartPosition = FormStartPosition.CenterScreen,
                    Visible = true
                };
                return;
            }

            if (!backgroundWorker_Xmpp.IsBusy)
            {
                backgroundWorker_Xmpp.RunWorkerAsync();
            }
        }

        void HandleMessageEvent(object sender, MessageEventArgs e)
        {
            switch (e.Action)
            {
                case "Login":
                    notifyIcon1.Icon = Resources.beeGreen;
                    notifyIcon1.Text = "PingAlert - Connected";
                    break;
                case "Ping":

                    if (Settings.Default.NotificationPriority){
                        Invoke(new Action(() =>
                        {
                            openMessageDialog(e.Message, e.Action, e.Sender);
                        }));
                    }
                    else
                    {
                        DateTimeOffset utcTime = DateTimeOffset.UtcNow;
                        notifyIcon1.BalloonTipIcon = ToolTipIcon.Info;
                        notifyIcon1.BalloonTipText = "There is an alert from command! Received : "+ utcTime.ToString("T");
                        notifyIcon1.BalloonTipTitle = e.Sender;
                        notifyIcon1.Tag = new
                        {
                            e.Sender,
                            e.Message,
                            e.Action
                        };
                        notifyIcon1.ShowBalloonTip(300000);
                    }
                    break;
                case "Error":
                    backgroundWorker_Xmpp.CancelAsync();

                    Invoke(new Action(() =>
                    {
                        openMessageDialog(e.Message, e.Action, e.Sender);
                    }));

                    break;
                case "AuthError":
                    backgroundWorker_Xmpp.CancelAsync();

                    Invoke(new Action(() =>
                    {
                        openMessageDialog(e.Message, e.Action, e.Sender);
                    }));

                    break;
                default:
                    break;
            }
        }

        private void disconnectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Debug.WriteLine("Request to disconnect!");
            client.Close();
            notifyIcon1.Icon = Resources.beeRed;
            notifyIcon1.Text = "PingAlert - Disconnected";
        }

        private void openMessageDialog(string message, string action, string sender)
        {
            Form errorDialog = new MessageDialog(message, action, sender)
            {
                Visible = true
            };
        }

        private void notifyIcon1_BalloonClick(object sender, EventArgs e)
        {
            dynamic value = sender;
            openMessageDialog(value.Tag.Message, value.Tag.Action, value.Tag.Sender);
        }

    }
}

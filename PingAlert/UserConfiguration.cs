﻿using PingAlert.Properties;
using System;
using System.Windows.Forms;

namespace PingAlert
{
    public partial class UserConfiguration : Form
    {
        public UserConfiguration()
        {
            InitializeComponent();
            TextBox objTextBoxUsername = (TextBox)textBox_Username;
            TextBox objTextBoxPassword = (TextBox)textBox_Password;
            ComboBox objComboBoxServer = (ComboBox)comboBox_Server;
            ComboBox objComboBoxPriority = (ComboBox)comboBox_Priority;
            ComboBox objComboBoxMessageTimer = (ComboBox)comboBox_MessageTimer;
            CheckBox objCheckboxStartup = (CheckBox)checkBox_Startup;
            CheckBox objCheckboxNotificationPriority = (CheckBox)checkBox_NotificationPriority;

            objComboBoxServer.SelectedIndex = Settings.Default.Server;
            objComboBoxPriority.SelectedIndex = Settings.Default.Priority;
            objTextBoxUsername.Text = Settings.Default.Username;
            objTextBoxPassword.Text = Settings.Default.Password;
            objComboBoxMessageTimer.SelectedIndex = Settings.Default.MessageTimer;
            objCheckboxStartup.Checked = Settings.Default.Startup;
            objCheckboxNotificationPriority.Checked = Settings.Default.NotificationPriority;

            Label objLabelError = (Label)label_Error;
            if (!String.IsNullOrEmpty(Main.SetValueForError))
            {
                objLabelError.Text = Main.SetValueForError;
            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            TextBox objTextBoxUsername = (TextBox)textBox_Username;
            TextBox objTextBoxPassword = (TextBox)textBox_Password;
            ComboBox objComboBoxServer = (ComboBox)comboBox_Server;
            ComboBox objComboBoxPriority = (ComboBox)comboBox_Priority;
            ComboBox objComboBoxMessageTimer = (ComboBox)comboBox_MessageTimer;
            CheckBox objCheckboxStartup = (CheckBox)checkBox_Startup;
            CheckBox objCheckboxNotificationPriority = (CheckBox)checkBox_NotificationPriority;
            Label objLabelError = (Label)label_Error;

            if (String.IsNullOrEmpty(objTextBoxUsername.Text))
            {
                objLabelError.Text = "Warning: Username is required!";
                return;
            }
            if (String.IsNullOrEmpty(objTextBoxPassword.Text))
            {
                objLabelError.Text = "Warning: Password is required!";
                return;
            }

            if (objCheckboxStartup.Checked)
            {
                Utility.EnableStartup();
            }
            else
            {
                Utility.DisableStartup();
            }

            objLabelError.Text = "";

            Settings.Default.Username = objTextBoxUsername.Text;
            Settings.Default.Password = objTextBoxPassword.Text;
            Settings.Default.Server = objComboBoxServer.SelectedIndex;
            Settings.Default.Priority = objComboBoxPriority.SelectedIndex;
            Settings.Default.MessageTimer = objComboBoxMessageTimer.SelectedIndex;
            Settings.Default.Startup = objCheckboxStartup.Checked;
            Settings.Default.NotificationPriority = checkBox_NotificationPriority.Checked;
            Settings.Default.Save();

            Main.SetValueForError = "";
            this.Close();
        }
    }
}

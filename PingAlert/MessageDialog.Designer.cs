﻿namespace PingAlert
{
    partial class MessageDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageDialog));
            this.button1 = new System.Windows.Forms.Button();
            this.richTextBox_message = new System.Windows.Forms.RichTextBox();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.progressBar_lifespan = new System.Windows.Forms.ProgressBar();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(136, 232);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Close";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // richTextBox_message
            // 
            this.richTextBox_message.BackColor = System.Drawing.Color.Black;
            this.richTextBox_message.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.richTextBox_message.ForeColor = System.Drawing.Color.White;
            this.richTextBox_message.Location = new System.Drawing.Point(12, 12);
            this.richTextBox_message.Name = "richTextBox_message";
            this.richTextBox_message.Size = new System.Drawing.Size(323, 214);
            this.richTextBox_message.TabIndex = 7;
            this.richTextBox_message.Text = "";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            // 
            // progressBar_lifespan
            // 
            this.progressBar_lifespan.ForeColor = System.Drawing.Color.Gold;
            this.progressBar_lifespan.Location = new System.Drawing.Point(235, 232);
            this.progressBar_lifespan.Name = "progressBar_lifespan";
            this.progressBar_lifespan.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.progressBar_lifespan.RightToLeftLayout = true;
            this.progressBar_lifespan.Size = new System.Drawing.Size(100, 23);
            this.progressBar_lifespan.Step = 1;
            this.progressBar_lifespan.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.progressBar_lifespan.TabIndex = 8;
            this.progressBar_lifespan.Value = 100;
            // 
            // MessageDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(346, 261);
            this.ControlBox = false;
            this.Controls.Add(this.progressBar_lifespan);
            this.Controls.Add(this.richTextBox_message);
            this.Controls.Add(this.button1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MessageDialog";
            this.Text = "Message";
            this.Load += new System.EventHandler(this.MessageDialog_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.RichTextBox richTextBox_message;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ProgressBar progressBar_lifespan;
    }
}